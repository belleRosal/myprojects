<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class App extends CI_Controller {

    function __construct() {
        parent::__construct();
		//$this->load->model('customer/mod_core');
		//$this->load->model('wallet/Mod_mywallet');
    }

    public function index() {
        if ($this->userauth->checkUserSession()) {

            $this->load->config("themes/{$this->session->userdata('theme')}");
			//$accountDetails = $this->mod_core->getAccountDetails($this->session->userdata('username'));
			
			
            //define layouts/themes
            $data['layout_js'] = $this->config->item('layout_js');
            $data['layout_css'] = $this->config->item('layout_css');
            $data['theme_js'] = $this->config->item('theme_js');
            $data['theme_css'] = $this->config->item('theme_css');

            //get allowed menus
            /*$header['menu'] = $this->mod_core->getPrivileges();
			$walletResult = $this->Mod_mywallet->getWalletAccount($this->session->userdata('username'));
			
			if($walletResult){
				$header['wallet']['wallet'] = $walletResult->wallet;
			} else {
				$header['wallet']['wallet'] = '0';
			}*/
			
            //header menu
            $data['header'] = $this->load->view('includes/header', $header, TRUE);

            //sidemenus
            $navbar['options'] = array();
            $data['navbar'] = $this->load->view('includes/navbar', $navbar, TRUE);

            //center content
            //$center['menu'] = $header['menu'];
            $data['center'] = $this->load->view('home', NULL, TRUE);

            //footer content
            $data['footer'] = $this->load->view('includes/footer', NULL, TRUE);

            $this->load->view('home', $data);
        } else {
            $this->login();
        }
    }

    public function login($status = null) {
        if ($this->session->userdata('session_key')) {
            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
                $this->reloadUrl('app/index');
            } else {
                redirect('app/index');
            }
        }
        $data['status'] = is_null($status) ? "" : $status;
        $this->load->view('login', $data);
    }

    public function doLogin() {
        if (strpos($this->input->post('username', TRUE), "\\")) {
            $filter_username = explode("\\", $this->input->post('username', TRUE));
            $username = $filter_username[0];
            $comp_code = $filter_username[1];
            $password_j = md5($this->input->post('password', TRUE) . HASH_KEY);
            $login_arr = array(
                'comp_code' => strtoupper($comp_code),
                'username' => $username,
                'password' => md5($this->input->post('password', TRUE) . HASH_KEY)
            );
            log_message('debug', 'DOLOGIN...');
            log_message('debug', print_r($login_arr, TRUE));
            $validate = $this->userauth->validateUser($login_arr);
            switch ($validate) {
                case 0:
                    $return = array(
                        'status_code' => 400,
                        'status_desc' => "Invalid Username or Password."
                    );
                    break;
                case 1:
                    $return = array(
                        'status_code' => 200,
                        'status_desc' => 'Success.',
                        'redirect' => 'app/index'
                    );
                    break;
                case 2:
                    $return = array(
                        'status_code' => 400,
                        'status_desc' => "SYSTEM UNDER MAINTENANCE."
                    );
                    break;
            }
           
        } else{
            $return = array(
                        'status_code' => 400,
                        'status_desc' => "Invalid Username or Password."
                        );
        }

        log_message('debug', print_r($return, TRUE));
        $this->activitylogger->loggedActivity($this->input->post('username', TRUE), 'Login Module', 'Authentication', 'Account Login', $return['status_desc']);

        echo json_encode($return);
    }

    public function reloadUrl($url) {
        if ($url = "expiredSession") {
            $url = 'app/login/expiredSession';
            $this->session->sess_destroy();
        }
        $data['url'] = site_url($url);
        $this->load->view('url_redirect', $data);
    }

    public function logout() {
        log_message('debug', 'DOLOGOUT');
        //activity logger
        $this->activitylogger->loggedActivity($this->session->userdata('username'), 'Logout Module', 'Authentication', 'Account Logout', 'Success.');
        $this->session->sess_destroy();
        redirect('app/login');
    }

}
