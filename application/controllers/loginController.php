<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loginController extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('username')) {
            redirect(base_url());
        }
		$this->load->view('login');
	}

	public function doLogin()
	{
		if ($this->session->userdata('username')) {
            redirect(base_url());
        }
		
		$post_data = array(
			'username' => filter_var($this->input->post('username'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH),
			'password' => md5(filter_var($this->input->post('password'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH))
		);
	

		$api_return = $this->api_caller->send_request( "POST", "loginAccount", "", json_encode($post_data));
		var_dump($api_return);

		if(!isset($api_return["statusCode"]))
		{
			$this->session->set_userdata($api_return);
			redirect(base_url());
		}
		else if($api_return["statusCode"] > 200 && $api_return["statusCode"] < 500)
		{
			redirect(base_url("Login"));
		}
		else
		{
			
		}
	}

	public function doLogout()
	{
		$this->session->sess_destroy();
        session_unset();
        $_SESSION = array();
        redirect(base_url());
	}
}
